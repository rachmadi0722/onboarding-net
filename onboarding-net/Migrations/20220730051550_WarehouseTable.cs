﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace onboarding_net.Migrations
{
    public partial class WarehouseTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "GudangId",
                table: "Kurir",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "WarehouseId",
                table: "Kurir",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Warehouses",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Address = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Warehouses", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Kurir_WarehouseId",
                table: "Kurir",
                column: "WarehouseId");

            migrationBuilder.AddForeignKey(
                name: "FK_Kurir_Warehouses_WarehouseId",
                table: "Kurir",
                column: "WarehouseId",
                principalTable: "Warehouses",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Kurir_Warehouses_WarehouseId",
                table: "Kurir");

            migrationBuilder.DropTable(
                name: "Warehouses");

            migrationBuilder.DropIndex(
                name: "IX_Kurir_WarehouseId",
                table: "Kurir");

            migrationBuilder.DropColumn(
                name: "GudangId",
                table: "Kurir");

            migrationBuilder.DropColumn(
                name: "WarehouseId",
                table: "Kurir");
        }
    }
}
