﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace onboarding_net.Migrations
{
    public partial class PerbaikiRelasi : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Kurir_Warehouses_WarehouseId",
                table: "Kurir");

            migrationBuilder.DropIndex(
                name: "IX_Kurir_WarehouseId",
                table: "Kurir");

            migrationBuilder.DropColumn(
                name: "WarehouseId",
                table: "Kurir");

            migrationBuilder.CreateTable(
                name: "KurirWarehouse",
                columns: table => new
                {
                    KurirId = table.Column<int>(type: "int", nullable: false),
                    WarehousesId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_KurirWarehouse", x => new { x.KurirId, x.WarehousesId });
                    table.ForeignKey(
                        name: "FK_KurirWarehouse_Kurir_KurirId",
                        column: x => x.KurirId,
                        principalTable: "Kurir",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_KurirWarehouse_Warehouses_WarehousesId",
                        column: x => x.WarehousesId,
                        principalTable: "Warehouses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_KurirWarehouse_WarehousesId",
                table: "KurirWarehouse",
                column: "WarehousesId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "KurirWarehouse");

            migrationBuilder.AddColumn<int>(
                name: "WarehouseId",
                table: "Kurir",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Kurir_WarehouseId",
                table: "Kurir",
                column: "WarehouseId");

            migrationBuilder.AddForeignKey(
                name: "FK_Kurir_Warehouses_WarehouseId",
                table: "Kurir",
                column: "WarehouseId",
                principalTable: "Warehouses",
                principalColumn: "Id");
        }
    }
}
