﻿using System.ComponentModel.DataAnnotations;

namespace onboarding_net.Models
{
    public class KurirGudang
    {
        public int IdGudang { get; set; }

        [Required]
        [StringLength(50)]
        public string NameKurir { get; set; }

        [Required]
        [StringLength(250)]
        public string AddressKurir { get; set; }
        public DateTime CreatedDateTime { get; set; } = DateTime.Now;

        [Required]
        public string NameGudang { get; set; }

        [Required]
        public string AddressGudang { get; set; }

    }
}
