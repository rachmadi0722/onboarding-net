﻿using System.ComponentModel.DataAnnotations;

namespace onboarding_net.Models
{
    public class Warehouse
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Address { get; set; }

        public List<Kurir>? Kurir { get; set; } = null;

    }
}
