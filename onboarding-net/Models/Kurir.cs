﻿using System.ComponentModel.DataAnnotations;

namespace onboarding_net.Models
{
    public class Kurir
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [Required]
        [StringLength(250)]
        public string Address { get; set; }
        public DateTime CreatedDateTime { get; set; } = DateTime.Now;

        public List<Item>? Items { get; set; } = null;

        public Warehouse? Warehouse { get; set; }

        public int GudangId { get; set; }


    }
}
