﻿using System.ComponentModel.DataAnnotations;
using System.Reflection;

namespace onboarding_net.Models
{
    public enum Status
    {
        Success,
        OnProgress
    }

    public class Item
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }
        public string Address { get; set; }

        public Status Status { get; set; }

        public Kurir? Kurir { get; set; }

        public int KurirId { get; set; }

    }
}
