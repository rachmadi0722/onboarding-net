﻿using MailKit.Net.Smtp;
using MailKit.Security;
using MimeKit;
using MimeKit.Text;

namespace onboarding_net.Email
{
    public class EmailHelper
    {
        public bool SendEmail(string userEmail, string confirmationLink)
        {

            var email = new MimeMessage();
            email.From.Add(MailboxAddress.Parse("cleora96@ethereal.email"));
            email.To.Add(MailboxAddress.Parse(userEmail));
            email.Subject = "Confirm your email";
            email.Body = new TextPart(TextFormat.Html) { Text = confirmationLink };

            using var smtp = new SmtpClient();
            smtp.Connect("smtp.ethereal.email", 587, SecureSocketOptions.StartTls);
            smtp.Authenticate("cleora96@ethereal.email", "mZ1duwyPqMUQDAzABG");
            var hasil = smtp.Send(email);
            smtp.Disconnect(true);
            return true;
        }
    }
}
