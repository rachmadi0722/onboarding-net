﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using onboarding_net.Data;
using onboarding_net.Models;
using System;
using System.Linq;
using System.Xml.Linq;

namespace onboarding_net.Controllers
{
    public class ItemController : Controller
    {
        private readonly ApplicationDbContext _db;

        public ItemController(ApplicationDbContext db)
        {
            _db = db;
        }

        public IActionResult Index(int? id)
        {
            IEnumerable<Item> objCategoryList = _db.Items.Where(m => m.KurirId == id);
            ViewBag.IdKurir = id;
            return View(objCategoryList);
        }

        //GET
        public IActionResult Create(int? id)
        {
            Item Data = new Item
            {
                KurirId = (int)id
            };
            return View(Data);
        }

        //POST
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create([Bind("Name,Address,KurirId,Status")] Item obj)
        {
            if (ModelState.IsValid)
            {
                _db.Items.Add(obj);
                _db.SaveChanges();
                return RedirectToAction("Index", new { id = obj.KurirId });
            }
            return View(obj);
        }
        //GET 
        public IActionResult Edit(int? id)
        {
            if (id == null || id == 0)
            {
                return NotFound();
            }
            var itemFromDb = _db.Items.Find(id);

            if (itemFromDb == null)
            {
                return NotFound();
            }

            return View(itemFromDb);
        }

        //PUT
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Item obj)
        {
            if (ModelState.IsValid)
            {
                _db.Items.Update(obj);
                _db.SaveChanges();
                return RedirectToAction("Index", new { id = obj.KurirId });
            }
            return View(obj);     
        }

        public IActionResult Delete(int? id)
        {
            if (id == null || id == 0)
            {
                return NotFound();
            }
            var itemFromDb = _db.Items.Find(id);

            if (itemFromDb == null)
            {
                return NotFound();
            }

            return View(itemFromDb);
        }

        //Delete
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeletePOST(int? id)
        {
            var obj = _db.Items.Find(id);
            if (obj == null)
            {
                return NotFound();
            }
  
            _db.Items.Remove(obj);
            _db.SaveChanges();
            return RedirectToAction("Index", new { id = obj.KurirId });

        }
    }
}
