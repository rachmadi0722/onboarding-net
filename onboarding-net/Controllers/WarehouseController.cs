﻿using onboarding_net.Data;
using Microsoft.AspNetCore.Mvc;
using onboarding_net.Models;
using System.Security.Policy;
using Microsoft.EntityFrameworkCore;

namespace onboarding_net.Controllers
{
    public class WarehouseController : Controller
    {
        private readonly ApplicationDbContext _db;

        public WarehouseController(ApplicationDbContext db)
        {
            _db = db;
        }

        public IActionResult Index()
        {
            IEnumerable<Warehouse> obj = _db.Warehouses;
            return View(obj);
        }

        //GET
        public IActionResult Create()
        {
            return View();
        }

        //POST
        [HttpPost]
        public IActionResult Create(Warehouse obj)
        {
            Console.WriteLine("Masuk Awal");
            if (ModelState.IsValid)
            {
                _db.Warehouses.Add(obj);
                
                _db.SaveChanges();
                Console.WriteLine("Masuk Akhir");
                return Json("Request Success");
            }
            Console.WriteLine("Tidak Masuk");
            return Json("Request Failed");
        }

        public IActionResult Multiple()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Multiple(KurirGudang obj)
        {
            Warehouse emp = new Warehouse();
            emp.Name = obj.NameGudang;
            emp.Address = obj.AddressGudang;

            _db.Warehouses.Add(emp);
            _db.SaveChanges();

            int latestWareId = emp.Id;


            Kurir site = new Kurir();
            site.Name = obj.NameKurir;
            site.Address = obj.AddressKurir;
            site.GudangId = latestWareId;

            _db.Kurir.Add(site);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }


        //GET 
        public IActionResult Edit(int? id)
        {
            if (id == null || id == 0)
            {
                return NotFound();
            }
            var categoryFromDb = _db.Warehouses.Find(id);

            if (categoryFromDb == null)
            {
                return NotFound();
            }
            return View(categoryFromDb);
        }

        //PUT
        [HttpPost]
        public IActionResult Edit(Warehouse obj)
        {
            if (ModelState.IsValid)
            {
                _db.Warehouses.Update(obj);
                _db.SaveChanges();
                return Json("Request Success");
            }
            return Json("Request Failed");
        }

        //Delete
        [HttpPost, ActionName("Delete")]
        public IActionResult Delete(Warehouse obj)
        {
            Console.WriteLine(obj.Id);
            var value = _db.Warehouses.Find(obj.Id);
            if (value == null)
            {
                return NotFound();
            }

            _db.Warehouses.Remove(value);
            _db.SaveChanges();
            return RedirectToAction("Index");

        }

    }
}
