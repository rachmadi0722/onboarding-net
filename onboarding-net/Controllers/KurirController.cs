﻿using ContosoUniversity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using onboarding_net.Data;
using onboarding_net.Models;
using System.Dynamic;

namespace onboarding_net.Controllers
{
    public class KurirController : Controller
    {
        private readonly ApplicationDbContext _db;

        public KurirController(ApplicationDbContext db)
        {
            _db = db;
        }

        public async Task<IActionResult> Index(string sortOrder, string currentFilter, string searchString, int? pageNumber)
        {
            ViewData["CurrentSort"] = sortOrder;
            ViewData["NameSortParm"] = String.IsNullOrEmpty(sortOrder) ? "Name" : "";
            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewData["CurrentFilter"] = searchString;

            var obj = from s in _db.Kurir
                      select s;
            if (!String.IsNullOrEmpty(searchString))
            {
                obj = obj.Where(s => s.Name.Contains(searchString) || s.Address.Contains(searchString));
            }
            switch (sortOrder)
            {
                case "Name":
                    obj = obj.OrderBy(s => s.Name);
                    break;
                default:
                    obj = obj.OrderBy(s => s.Address);
                    break;
            }

            int pageSize = 2;
            return View(await PaginatedList<Kurir>.CreateAsync(obj.AsNoTracking(), pageNumber ?? 1, pageSize));
        }

        /*public IActionResult Index(string? Search, int? pageNumber)
        {
            ViewData["CurrentFilter"] = Search;
            IEnumerable<Kurir> objCategoryList = _db.Kurir;
            if (!String.IsNullOrEmpty(Search))
            {
                objCategoryList = _db.Kurir.Where(k => k.Name.Contains(Search) || k.Address.Contains(Search));
            }
            int pageSize = 3;
            return View(objCategoryList.PaginatedList();
        }*/

        // GET
        public IActionResult Create()
        {
            ViewBag.Warehouse = _db.Warehouses;
            return View();
        }

        //POST
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Kurir obj)
        {
            if (ModelState.IsValid)
            {
                _db.Kurir.Add(obj);
                _db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(obj);
        }

        //GET 
        public IActionResult Edit(int? id)
        {
            if (id == null || id == 0)
            {
                return NotFound();
            }
            var categoryFromDb = _db.Kurir.Find(id);

            if (categoryFromDb == null)
            {
                return NotFound();
            }
            ViewBag.Warehouse = _db.Warehouses;
            return View(categoryFromDb);
        }

        //PUT
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Kurir obj)
        {
            if (ModelState.IsValid)
            {
                _db.Kurir.Update(obj);
                _db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(obj);
        }

        public IActionResult Delete(int? id)
        {
            if (id == null || id == 0)
            {
                return NotFound();
            }
            var categoryFromDb = _db.Kurir.Find(id);

            if (categoryFromDb == null)
            {
                return NotFound();
            }
            ViewBag.Warehouse = _db.Warehouses;
            return View(categoryFromDb);
        }

        //Delete
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeletePOST(int? id)
        {
            var obj = _db.Kurir.Find(id);
            if (obj == null)
            {
                return NotFound();
            }

            _db.Kurir.Remove(obj);
            _db.SaveChanges();
            return RedirectToAction("Index");

        }
    }
}
